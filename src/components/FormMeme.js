import React, { Fragment, useState } from 'react';
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import Menu from './Menu';

export default function FormMeme({ memes, setMemes }) {
    
    // Definimos el state para meme
    const [meme, setMeme] = useState({
        titulo: '',
        url: '',
        imgBase64: null
    });
    
    // Extraemos del meme
    const { titulo, url, imgBase64 } = meme;
    
    // Cuando hay cambios en el formulario
    const onChangeMeme = e => {
        setMeme({
            ...meme,
            [e.target.name]: e.target.value
        });
    };

    const onChangeMemeImg = async e => {
        if(e.target.files[0]){
            if(e.target.files[0].size > 4194304){
                // 5242880 = 5MB
                // 4194304 = 4MB
                e.target.value = null;
                alert('La imagen es demasiado grande.');
                setMeme({
                    ...meme,
                    imgBase64: null
                });
                return;
            }
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onloadend = () => {
                setMeme({
                    ...meme,
                    imgBase64: reader.result
                });
            };
        } else {
            setMeme({
                ...meme,
                imgBase64: null
            });
        }
    };
    
    // Cuando se crea un meme
    const onSubmitNuevoMeme = async e => {
        e.preventDefault();
    
        // Validar campos
        if(titulo.trim() === '' || url.trim() === '' || imgBase64 === null){
            alert('Todos los campos son obligatorios');
            return;
        }
    
        // Agregar meme
        try {
            const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/memes/',{
                method: 'POST',
                body: JSON.stringify(meme),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const respuesta = await solicitud.json();

            if(solicitud.ok){
                setMemes([
                    ...memes,
                    respuesta.meme
                ]);
                alert(respuesta.msg);
                // Resetear el formulario
                setMeme({
                    titulo: '',
                    url: '',
                    imgBase64: null
                });
                document.getElementById('img').value = "";
            } else {
                alert(respuesta.msg);
            }
        } catch (error) {
            alert('Hubo un error!');
        }        
    }

    
    return (
        <Fragment>
            <Menu />
            <main>
                <Container>
                    <h3 className="text-center mt-3">Agregar nuevo meme</h3>
                    <Row>
                        <Col xs={12} sm={8} ms={6} className="mx-auto">
                            <Card bg="light">
                                <Card.Body>
                                <Form onSubmit={onSubmitNuevoMeme}>
                                    <Form.Group controlId="titulo">
                                        <Form.Label>Título</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="titulo"
                                            onChange={onChangeMeme}
                                            value={titulo}
                                        ></Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="url">
                                        <Form.Label>URL</Form.Label>
                                        <Form.Control
                                            type="text"
                                            name="url"
                                            onChange={onChangeMeme}
                                            value={url}
                                        ></Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="img">
                                        <Form.Label>Imagen</Form.Label>
                                        <Form.File 
                                            id="img"
                                            accept="image/*"
                                            onChange={onChangeMemeImg}
                                        />
                                    </Form.Group>
                                    <Button type="submit">Agregar</Button>
                                </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </main>
        </Fragment>
    )
}