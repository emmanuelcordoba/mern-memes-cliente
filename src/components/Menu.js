import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Menu() {
    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand>Memes</Navbar.Brand>
            <Nav>
                <Link to="/agregar-meme" className="nav-link">Agregar Meme</Link>
                <Link to="/" className="nav-link">Listado</Link>
            </Nav>
        </Navbar>
    )
}