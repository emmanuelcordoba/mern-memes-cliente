import React, { Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Menu from './Menu';
import Meme from './Meme';

export default function ListadoMemes({ memes, setMemes }) {
    return (
        <Fragment>
            <Menu />
            <main>
                <Container>
                    <h3 className="text-center mt-3">Listado de memes</h3>
                    <Row className="row-cols-1 row-cols-sm-2 row-cols-md-4">
                        {
                            memes.map(meme => (
                                <Col key={meme._id}>
                                    <Meme 
                                        meme={meme}
                                        setMemes={setMemes}
                                    />
                                </Col>
                            ))
                        }
                        
                    </Row>
                </Container>
            </main>
        </Fragment>
    )
}
