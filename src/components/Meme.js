import React from 'react';
import { Card, Button } from 'react-bootstrap';

export default function Meme({ meme, setMemes }) {
    
    // Eliminar el meme
    const onClickEliminar = async () => {
        if(window.confirm('¿Está seguro que desea eliminar el meme?')){
            try {
                const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/memes/'+meme._id, {
                    method: 'DELETE'
                });
                const respuesta = await solicitud.json();
                alert(respuesta.msg);
                if(solicitud.ok){
                    actualziarMemes();
                }
            } catch (error) {
                alert('Hubo un error');
            }
        }
    }

    const actualziarMemes = async () => {
        const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/memes/');
        const respuesta = await solicitud.json();

        if(solicitud.ok){
            setMemes(respuesta.memes);
        } else {
            alert(respuesta.msg);
        }
    }

    
    //const bytes = new Uint8Array(meme.img.data.data);
    

    return (
        <Card className="mb-3">
            <Card.Img variant="top" src={meme.imgBase64} />
            <Card.Body>
                <Card.Title>{ meme.titulo }</Card.Title>
                <Button 
                    variant="primary" 
                    size="sm" 
                    href={meme.url} 
                    target="_blank"
                    className="mr-1"
                >Ver</Button>
                <Button 
                    variant="danger" 
                    size="sm"
                    onClick={onClickEliminar}
                >Eliminar</Button>
            </Card.Body>
        </Card>
    )
}
