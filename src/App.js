import React, { useState, useEffect } from 'react';
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import ListadoMemes from './components/ListadoMemes';
import FormMeme from './components/FormMeme';

function App() {
  
  const [memes, setMemes] = useState([]);

  useEffect( async () => {
    const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/memes/');
    const respuesta = await solicitud.json();

    if(solicitud.ok){
        setMemes(respuesta.memes);
    } else {
        alert(respuesta.msg);
    }
  }, [])
  
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <ListadoMemes memes={memes} setMemes={setMemes} />
        </Route>
        <Route exact path="/agregar-meme">
          <FormMeme memes={memes} setMemes={setMemes} />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
